# DSA Chroniken

## Motivation

[Zakkarus](https://www.orkenspalter.de/index.php/User/11132-zakkarus/) hat mit
seinen wunderbaren Chroniken ('Aventurien damals') eine hervorragende Arbeit geleistet.

Meine Kritik betrifft vor allem das Layout, welches für mich zu aufgeregt und inkonsistent ist.
Da meckern immer das einfachste ist, will ich es nicht dabei belassen und versuche
mich an einem kleinen Projekt mit der vorhandenen Datenbasis.

Der wichtigste Schritt ist die Auslagerung der Ereignisinformationen und damit
die Trennung von Daten und Layout. Ich hoffe die Daten in Zukunft auch für andere
interessante Dinge verwenden zu können.

Das benötigte Programm findet sich [hier](https://gitlab.com/thallian/chronicles-creator/import)
